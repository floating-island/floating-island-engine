//var settings = require('./settings.js');

const express = require('express');
const bodyParser = require('body-parser');
const path = require('path');

const app = new express();
const server = require('http').Server(app);

const io = require('socket.io')(server);

const Shard = require('./shard/server.js').server;
let shard = new Shard(io);

const staticFiles = express.static(__dirname + "/static");
const staticSocketClient = express.static(__dirname + "/node_modules/socket.io-client/dist");
app.use(staticFiles);
app.use(staticSocketClient);
const urlEncoded = bodyParser.urlencoded({
    extended: true
});
app.use(urlEncoded);

app.get("/", (request, response) => {
    response.sendFile(__dirname + "/static/index.html");
});

app.get("/game", (request, response) => {
    response.sendFile(__dirname + "/static/game.html");
});

app.post("/login", (request, response) => {
    // Login should be done here
});

app.post("/signup", (request, response) => {
    // Signup should be done here.
});

server.listen(3000, () => {
  console.log('listening on *:3000');
});
