const Player = require('./shard/player.js').player;
const Room = require('./shard/room.js').room;
const createRooms = require('./serverContent/rooms.js').createRooms;

exports.server = class {
    constructor(io) {
        let self = this;
        this.rooms = {};
        this.io = io;
        this.io.on('connection', (socket)=>{self.establishCon(socket);});
        createRooms(this);
        this.clients = {};
    }

    establishCon(socket) {
        // TODO: Add max clients per server instance here.
        new Player(this, socket);
    }

    firstRoom(client, room) {
        let randomRoom;
        if(this.rooms[room]){
            randomRoom = room;
        } else {
            let rooms = Object.keys(this.rooms);
            randomRoom = rooms[ rooms.length * Math.random() << 0];
        }
        if(this.rooms[randomRoom].canJoin()){
            this.rooms[randomRoom].joinRoom(client)
        } else {
            this.firstRoom(client, null);
        }
    }

    moveRoom(client, room) {
        console.log(room);
        if(this.rooms[room]){
            console.log(Object.keys(this.rooms));
            this.rooms[room].joinRoom(client)
        } else {
            client.socket.emit("orangeBox", "Room doesn't exsist");
        }
    }
}
