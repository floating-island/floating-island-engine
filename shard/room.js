exports.room = class {
    constructor(server, roomName, xTP, yTP) {
        this.server = server;
        this.server.rooms[roomName] = this;
        this.name = roomName;
        this.playerCount = 0;
        this.players = {};
        this.maxPlayers = 2;
        this.autoJoin = true;
        this.startFrom = {};
        this.center = {"x":xTP,"y":yTP};
    }

    startFrom(room, x, y) {
        this.startFrom[room] = {"x":x,"y":y};
    }

    message(uuid, message) {
        this.server.io.to(this.name).emit("message", {uuid, message});
    }

    walk(socket, uuid, dest) {
        if(dest.x && dest.y){
            if(dest.x > 0 && dest.y > 0 && dest.x < 100 && dest.y < 100){
                this.server.io.to(this.name).emit("walk", {uuid, dest});
            }
        }
    }

    leaveRoom(client) {
        client.socket.to(this.name).emit("left", client.uuid);
        client.socket.leave(this.name);
        delete this.players[client.uuid];
        this.playerCount --;
    }

    canJoin() {
        return (this.playerCount <= this.maxPlayers);
    }

    joinRoom(client, teleport) {
        if(this.canJoin()){
            this.playerCount ++;
            let lastRoom = "none";
            if(client.room){
                lastRoom = client.room.name;
                client.room.leaveRoom(client);
            }
            client.socket.join(this.name);
            client.room = this;
            if(!teleport && this.startFrom[lastRoom]){
                client.x = this.startFrom[lastRoom].x;
                client.y = this.startFrom[lastRoom].y;
            } else {
                client.x = this.center.x;
                client.y = this.center.y;
            }
            this.players[client.uuid] = client;
            let players = []
            for(let p of this.players){
                players.push(p.currentLocation());
            }
            client.socket.emit("moved", {"name":this.name,"players":players});
            client.socket.to(this.name).emit("joined", {"uuid":client.uuid, "x":x, "y":y});
        } else {
            client.socket.emit("orangeBox", "The Room is Full");
        }
    }
}
