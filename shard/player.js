exports.player = class {
    constructor(server, socket, x, y) {
        this.server = server;
        this.socket = socket;
        this.authed = 0;
        this.name = "???";
        this.uuid = null;
        this.room = null;
        this.startListeners();
        this.x = x;
        this.y = y;
    }

    startListeners() {
        let self = this;
        this.socket.on('disconnect', ()=>{self.disconnect();});

        this.socket.on('auth', (data)=>{self.authenacate(data);});
        this.socket.emit('sendAuth');
        // We are ready for a auth message.

        this.socket.on('message', (message)=>{self.message(message);});
        // Chat messages
        this.socket.on('walk', (dest)=>{self.walk(dest);});
        // In-Room Walks

        this.socket.on('move', (dest)=>{self.move(dest);});
        // Between Room walk
        //this.socket.on('event', ()=>{self.authenacate();});
        // Example event for copying.


        setTimeout(()=>{self.noAuth();}, 10000);
        // Max of 10s wait for auth message;
    }

    disconnect() {
        if(this.uuid){
            if(this.server.clients[this.uuid]){
                delete this.server.clients[this.uuid];
            }
        }
        if(this.room){
            this.room.leaveRoom(this);
        }
    }

    noAuth() {
        if(this.authed == 0){
            console.log("Kicked due to lack of auth");
            this.socket.emit("orangeBox", "Kicked due to invalid Auth.");
            this.socket.disconnect();
        }
    }

    authenacate(data) {
        if(this.authed == 0){
            this.authed = 1;
            this.name = data.name;
            this.uuid = data.uuid;
            this.room = null;
            this.server.firstRoom(this, data.room);
            this.server.clients[this.uuid] = this;
        }
    }

    message(message){
        console.log("Got message");
        this.room.message(this.uuid, message);
    }

    walk(dest){
        console.log("Walked");
        this.x = dest.x;
        this.y = dest.y;
        this.room.walk(this.socket, this.uuid, dest);
    }

    currentLocation() {
        return {"uuid": this.uuid, "name":this.name, "x": this.x, "y": this.y};
    }

    move(dest){
        console.log("Moved");
        console.log(dest);
        this.server.moveRoom(this, dest);
    }
}
