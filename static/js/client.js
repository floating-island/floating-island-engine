// When socket.io finally can be ES6 imported, import it here.
import Room from "./room.js";

class Client {
    constructor(serverIP, username, uuid) {
        this.socket = io(serverIO, {
            reconnection: false,
            autoConnect: false
        });
        this.username = username;
        this.uuid = uuid;
        this.room = null;
    }

    setListeners() {
        let self = this;
        this.socket.on('sendAuth', ()=>{self.sendAuth();});

        this.socket.on('disconnect', (reason)=>{self.orangeBox("Disconnected");});

        this.socket.on('orangeBox', (error)=>{self.orangeBox(error);});

        this.socket.on('moved', (roomData)=>{self.moved(roomData);});

        this.socket.connect();
    }

    sendAuth() {
        socket.emit("auth", {"name":this.username,"uuid":this.uuid});
    }

    moved(roomData) {
        if(this.room){
            this.room.destorySelf();
        }
        this.room = new Room(this.socket, roomData);
    }

    orangeBox(message) {
        console.warn(message);
    }
}

const server = "127.0.0.2:3000";

socket.on('message', (data)=>{
    console.log(data.uuid + " said " + data.message);
});

window.message = function(message) {
    socket.emit('message', message);
};

socket.on('walk', (data)=>{
    console.log(data.uuid + " walked to " + JSON.stringify(data.dest));
});

window.walk = function(x, y) {
    socket.emit('walk', {x, y});
};

socket.on('joined', (initialData)=>{
    console.log(initialData.uuid + " Joined room");
});

socket.on('left', (uuid)=>{
    console.log(uuid + " Left room");
});

window.move = function(room, teleport) {
    socket.emit('move', {room, teleport});
};
